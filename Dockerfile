FROM almalinux:9.5

LABEL maintainer='Mike Newton <jmnewton@duke.edu>'

RUN dnf install -y epel-release && dnf config-manager --set-enabled crb

RUN dnf groupinstall -y 'Development tools' && \
    dnf install -y --allowerasing rpm-build http-parser-devel json-c-devel munge-devel munge-libs pam-devel readline-devel munge mariadb mariadb-devel hwloc lua glibc-devel numactl-devel numactl libjwt-devel libjwt libyaml libyaml-devel hdf5 hdf5-devel lua-devel man2html libcurl libcurl-devel perl-ExtUtils-MakeMaker dbus-devel

RUN curl -L https://gitlab.com/gitlab-org/cli/-/releases/v1.39.0/downloads/glab_1.39.0_Linux_x86_64.rpm -o glab_1.39.0_Linux_x86_64.rpm && rpm -Uvh glab_1.39.0_Linux_x86_64.rpm

ENTRYPOINT ["/bin/bash", "-c"]
